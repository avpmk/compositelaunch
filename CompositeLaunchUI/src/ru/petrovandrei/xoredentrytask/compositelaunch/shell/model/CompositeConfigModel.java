package ru.petrovandrei.xoredentrytask.compositelaunch.shell.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import ru.petrovandrei.common.collections.readonlymap.ReadOnlyMap;
import ru.petrovandrei.common.eclipse.launch.ConfigUtil;
import ru.petrovandrei.common.eclipse.launch.UncheckedCoreException;
import ru.petrovandrei.common.fi.BiConsumerThr;
import ru.petrovandrei.common.fi.BiPredicateThr;
import ru.petrovandrei.common.fi.PredicateThr;
import ru.petrovandrei.common.util.IterableIterator;
import ru.petrovandrei.xoredentrytask.compositelaunch.Messages;
import ru.petrovandrei.xoredentrytask.compositelaunch.shell.contoller.IConfigsTreeController;
import ru.petrovandrei.xoredentrytask.compositelaunch.shell.contoller.NullIConfigsTreeController;
import ru.petrovandrei.xoredentrytask.compositelaunch.util.ConfigsTreeWalker;

import static java.util.Arrays.stream;
import static ru.petrovandrei.common.eclipse.launch.ConfigUtil.mapOfLaunchConfigurations;
import static ru.petrovandrei.common.util.IterableIterator.asIterableIterator;
import static ru.petrovandrei.xoredentrytask.compositelaunch.Activator.newStatusError;
import static ru.petrovandrei.xoredentrytask.compositelaunch.util.CompositeConfigUtil.configsTree;
import static ru.petrovandrei.xoredentrytask.compositelaunch.util.CompositeConfigUtil.getNestedConfigNames;
import static ru.petrovandrei.xoredentrytask.compositelaunch.util.CompositeConfigUtil.isComposite;
import static ru.petrovandrei.xoredentrytask.compositelaunch.util.CompositeConfigUtil.setNestedConfigNames;

public class CompositeConfigModel {

    private final Set<String>
            namesToAdd    = new HashSet<>(),
            namesToRemove = new HashSet<>();

    private ILaunchConfiguration configComposite;

    public String getConfigName() {
        return configComposite.getName();
    }

    public void setConfigComposite(ILaunchConfiguration config) throws CoreException {
        try {
            while (config.isWorkingCopy()) {
                config = ((ILaunchConfigurationWorkingCopy) config).getOriginal();
            }

            if (!isComposite(config)) {
                throw new CoreException(
                        newStatusError(Messages.configNotComposite(config))
                );
            }

            configComposite = config;
        } finally {
            namesToAdd.clear();
            namesToRemove.clear();
        }
    }

    public void add(String name) throws CoreException {
        if (!namesToRemove.remove(name)) {
            namesToAdd.add(name);
        }
    }

    public void remove(String name) {
        if (!namesToAdd.remove(name)) {
            namesToRemove.add(name);
        }
    }

    public void saveTo(ILaunchConfigurationWorkingCopy configComposite) throws CoreException {
        setNestedConfigNames(configComposite, nestedConfigNames_withModifications());
    }

    private ArrayList<String> nestedConfigNames_withModifications() throws CoreException {
        ArrayList<String> names = new ArrayList<>(getNestedConfigNames(configComposite));
        names.removeAll(namesToRemove);
        names.addAll(namesToAdd);
        return names;
    }

    private IConfigsTreeController controller = new NullIConfigsTreeController();

    public void setConfigsTreeController(IConfigsTreeController controller) {
        this.controller = controller;
    }

    private FiltationModeAllConfigs filtationModeAllConfigs = FiltationModeAllConfigs.INCLUDED_ONLY_TO_CURRENT;

    public void setFiltationModeAllConfigs(FiltationModeAllConfigs filtationModeAllConfigs) {
        if (this.filtationModeAllConfigs != filtationModeAllConfigs) {
            this.filtationModeAllConfigs = filtationModeAllConfigs;
            controller.updateTreeConfigsToAdd();
        }
    }

    private FiltationModeCompositeConfigs filtationModeCompositeConfigs = FiltationModeCompositeConfigs.NO_FILTRATION;

    public void setFiltationModeCompositeConfigs(FiltationModeCompositeConfigs filtationModeCompositeConfigs) {
        if (this.filtationModeCompositeConfigs != filtationModeCompositeConfigs) {
            this.filtationModeCompositeConfigs = filtationModeCompositeConfigs;
            controller.updateTreeConfigsToAdd();
        }
    }

    //todo rename
    public CompositeConfigTreeOpertations treeOpertations() throws CoreException {
        CompositeConfigTreeOpertations treeOpertations = new CompositeConfigTreeOpertations();
        filtationModeAllConfigs.applyTo(treeOpertations);
        filtationModeCompositeConfigs.applyTo(treeOpertations);
        return treeOpertations;
    }

    //todo rename
    public class CompositeConfigTreeOpertations {

        private final ReadOnlyMap<String, ILaunchConfiguration> mapLaunchConfigs;
        private final ConfigsTreeWalker configsTree;

        //<editor-fold defaultstate="collapsed" desc="blockingByCommonConfigsFilter">
        private PredicateThr<String, CoreException> blockedByCommonConfigsFilter;

        void applyCommonConfigsFilter_includedOnlyToCurrent() {
            blockedByCommonConfigsFilter = name -> nestedConfigNames.contains(name);
        }

        void applyCommonConfigsFilter_includedToCurrentOrAnySubconfig() {
            blockedByCommonConfigsFilter = name -> configsTree.matchAny(nestedConfigNames, name::equals);
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="blockingByCompositeConfigsFilter">
        private BiPredicateThr<ILaunchConfiguration, String, CoreException> blockedByCompositeConfigsFilter;

        void applyCompositeConfigsFilter_containsCurrent() {
            blockedByCompositeConfigsFilter = (configCompositeRoot, nameToTest)
                    -> isComposite(configCompositeRoot) && getNestedConfigNames(configCompositeRoot).contains(nameToTest);
        }

        void applyCompositeConfigsFilter_containsCurrentAnyDeep() {
            blockedByCompositeConfigsFilter = (configCompositeRoot, nameToTest)
                    -> isComposite(configCompositeRoot) && configsTree.matchAny(configCompositeRoot, nameToTest::equals);
        }

        void applyCompositeConfigsFilter_noFiltration() {
            blockedByCompositeConfigsFilter = (configCompositeRoot, nameToTest)
                    -> false;
        }
        //</editor-fold>

        private final List<String> nestedConfigNames;

        private CompositeConfigTreeOpertations() throws CoreException {
            this.mapLaunchConfigs = mapOfLaunchConfigurations();
            this.configsTree = configsTree(mapLaunchConfigs::get);

            if (namesToAdd.isEmpty() && namesToRemove.isEmpty()) {
                nestedConfigNames = getNestedConfigNames(configComposite);
            } else {
                nestedConfigNames = nestedConfigNames_withModifications();
            }
        }

        public IterableIterator<ILaunchConfiguration> configsThatCanBeAddedToThisConfig(ILaunchConfigurationType type) throws CoreException {
            return asIterableIterator(
                    stream(ConfigUtil.configs(type)).filter(this::canBeAddedToThisConfig)
            );
        }

        private boolean canBeAddedToThisConfig(ILaunchConfiguration configToTest) throws UncheckedCoreException {
            String
                    nameToTest = configToTest.getName(),
                    nameRoot   = configComposite.getName();

            try {
                return
                        !nameToTest.equals(nameRoot) &&
                        !blockedByCommonConfigsFilter.test(nameToTest) &&
                        !blockedByCompositeConfigsFilter.test(configToTest, nameRoot);
            } catch (CoreException ex) {
                throw new UncheckedCoreException(ex);
            }
        }

        public void forAllNestedConfigs_uniqNames(BiConsumerThr<ILaunchConfiguration, String, CoreException> action) throws CoreException {
            configsTree.foreachLeaf(
                    nestedConfigNames, action
            );
        }

        //<editor-fold defaultstate="collapsed" desc="dfs">
        public <T> void fillTree(
                Supplier<T> rootChildMaker, //marked as moveable
                TreeNodeActions<T> actions
        ) throws CoreException {
            SinglyLinkedNode<String> nodeRoot = new SinglyLinkedNode<>(configComposite.getName());
            for (String name : nestedConfigNames) {
                fillWholeTree(rootChildMaker.get(), nodeRoot.createChild(name), actions);
            }
        }

        private <T> void fillWholeTree(
                T treeItem,
                SinglyLinkedNode<String> fullQualifiedName,
                TreeNodeActions<T> actions
        ) throws CoreException {
            actions.initialize(treeItem, fullQualifiedName);
            String name = fullQualifiedName.value;
            ILaunchConfiguration config = mapLaunchConfigs.get(name);
            if (config == null) {
                actions.applyRemovedState(treeItem, name);
            } else {
                if (isComposite(config)) {
                    if (fullQualifiedName.hasDuplicates()) {
                        actions.applyRecursiveState(treeItem);
                    } else {
                        actions.applyNormalState(treeItem, config);
                        for (String nestedConfigName : getNestedConfigNames(config)) {
                            fillWholeTree(actions.createChild(treeItem), fullQualifiedName.createChild(nestedConfigName), actions);
                        }
                    }
                } else {
                    actions.applyNormalState(treeItem, config);
                }
            }
        }
        //</editor-fold>
    }
}