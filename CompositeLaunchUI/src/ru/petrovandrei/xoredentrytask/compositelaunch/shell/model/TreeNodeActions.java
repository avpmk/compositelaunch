package ru.petrovandrei.xoredentrytask.compositelaunch.shell.model;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;

public interface TreeNodeActions<T> {

    T createChild(T treeItem);
    void initialize(T treeItem, SinglyLinkedNode<String> fullQualifiedName) throws CoreException;
    void applyNormalState(T treeItem, ILaunchConfiguration config) throws CoreException;
    void applyRemovedState(T treeItem, String name);
    void applyRecursiveState(T treeItem);
}