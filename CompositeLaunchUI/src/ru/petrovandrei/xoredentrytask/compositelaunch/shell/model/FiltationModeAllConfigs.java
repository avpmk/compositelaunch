package ru.petrovandrei.xoredentrytask.compositelaunch.shell.model;

import ru.petrovandrei.xoredentrytask.compositelaunch.shell.model.CompositeConfigModel.CompositeConfigTreeOpertations;

public enum FiltationModeAllConfigs {

    INCLUDED_ONLY_TO_CURRENT {
        @Override public void applyTo(CompositeConfigTreeOpertations treeOpertations) {
            treeOpertations.applyCommonConfigsFilter_includedOnlyToCurrent();
        }
    },
    INCLUDED_TO_CURRENT_OR_ANY_SUBCONFIG {
        @Override public void applyTo(CompositeConfigTreeOpertations treeOpertations) {
            treeOpertations.applyCommonConfigsFilter_includedToCurrentOrAnySubconfig();
        }
    };

    public abstract void applyTo(CompositeConfigTreeOpertations treeOpertations);
}