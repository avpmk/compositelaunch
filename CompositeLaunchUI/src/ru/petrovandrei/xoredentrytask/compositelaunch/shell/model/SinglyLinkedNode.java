package ru.petrovandrei.xoredentrytask.compositelaunch.shell.model;

public class SinglyLinkedNode<E> {

    private SinglyLinkedNode<E> parent;
    public final E value;

    public SinglyLinkedNode(E value) {
        this.parent = null;
        this.value = value;
    }

    private SinglyLinkedNode(SinglyLinkedNode<E> prev, E value) {
        this.parent = prev;
        this.value = value;
    }

    public SinglyLinkedNode<E> createChild(E element) {
        return new SinglyLinkedNode<>(this, element);
    }

    public SinglyLinkedNode<E> createParent(E element) {
        SinglyLinkedNode<E> nodeParent = new SinglyLinkedNode<>(element);
        this.parent = nodeParent;
        return nodeParent;
    }

    public boolean hasDuplicates() {
        SinglyLinkedNode<E> node = parent;
        while (node != null) {
            if (value.equals(node.value)) {
                return true;
            }
            node = node.parent;
        }
        return false;
    }

    @Override public boolean equals(Object o) {
        return o instanceof SinglyLinkedNode && sizeAndValuesOfChainsEquals((SinglyLinkedNode<?>) o);
    }

    /**
     * @throws NullPointerException если this.value == null или value одного из parent'ов (of this) == null
     */
    private boolean sizeAndValuesOfChainsEquals(SinglyLinkedNode<?> anotherChainNode) {
        SinglyLinkedNode<E> thisChainNode = this;
        boolean thisChainFinished, anotherChainFinished;
        do {
            if (!eq(thisChainNode.value, anotherChainNode.value)) {
                return false;
            }
            thisChainNode = thisChainNode.parent;
            anotherChainNode = anotherChainNode.parent;

            thisChainFinished = thisChainNode == null;
            anotherChainFinished = anotherChainNode == null;

            if (thisChainFinished && anotherChainFinished) {
                return true;
            }

        } while (!thisChainFinished && !anotherChainFinished);
        return false;
    }
    
    @Override public int hashCode() {
        return value.hashCode();
    }

    @Override public String toString() {
//        if (parent != null) {
//            return value + " -> " + parent;
//        } else {
        return String.valueOf(value);
//        }
    }

    private static <T> boolean eq(T a, T b) {
        return a.equals(b);
    }
}