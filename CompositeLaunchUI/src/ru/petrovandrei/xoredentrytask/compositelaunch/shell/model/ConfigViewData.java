package ru.petrovandrei.xoredentrytask.compositelaunch.shell.model;

import org.eclipse.swt.graphics.Image;
import ru.petrovandrei.common.util.swt.TableData;

public class ConfigViewData implements TableData {

    public final String name;
    public final Image image;

    public ConfigViewData(String name, Image image) {
        this.name = name;
        this.image = image;
    }

    //<editor-fold defaultstate="collapsed" desc="TableData impl">
    @Override public String name() {
        return name;
    }

    @Override public Image image() {
        return image;
    }
    //</editor-fold>

    @Override public int hashCode() {
        return name.hashCode();
    }

    @Override public boolean equals(Object o) {
        return o instanceof ConfigViewData && ((ConfigViewData) o).name.equals(name);
    }
}