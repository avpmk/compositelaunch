package ru.petrovandrei.xoredentrytask.compositelaunch.shell.model;

import ru.petrovandrei.xoredentrytask.compositelaunch.shell.model.CompositeConfigModel.CompositeConfigTreeOpertations;

public enum FiltationModeCompositeConfigs {

    CONTAINS_CURRENT {
        @Override public void applyTo(CompositeConfigTreeOpertations treeOpertations) {
            treeOpertations.applyCompositeConfigsFilter_containsCurrent();

        }
    },
    CONTAINS_CURRENT_ANY_DEEP {
        @Override public void applyTo(CompositeConfigTreeOpertations treeOpertations) {
            treeOpertations.applyCompositeConfigsFilter_containsCurrentAnyDeep();
        }
    },
    NO_FILTRATION {
        @Override public void applyTo(CompositeConfigTreeOpertations treeOpertations) {
            treeOpertations.applyCompositeConfigsFilter_noFiltration();
        }
    };

    public abstract void applyTo(CompositeConfigTreeOpertations treeOpertations);
}