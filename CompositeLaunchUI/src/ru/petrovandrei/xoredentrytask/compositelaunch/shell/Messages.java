package ru.petrovandrei.xoredentrytask.compositelaunch.shell;

import org.eclipse.osgi.util.NLS;

import static java.text.MessageFormat.format;

public class Messages extends NLS {

    public static String
            tabName,
            tabMessage,
            configsDoesntExist,
            failedToSave,
            errorOnFormInitialization,

            addSelectedConfigs,
            createNotExistedConfig,
            editConfig,
            removeSelectedConfigs,

            filtrationModes,
            filtrateAll,
            filtrateConfigsWhichIncludedToCurrent,
            filtrateConfigsWhichIncludedToCurrentOrAnySubconfig,

            filtrateComposite,
            filtrateCompositeConfigsWhichContainsCurrent,
            filtrateCompositeConfigsWhichContainsCurrent_orAnySubconfigContainsCurrent,
            dontFiltrateCompositeConfigs,

            showNestedConfigsHierarchicallyOrOnlyThatWillBeStarted,

            configsToAdd,
            configsAdded,
            configsToStart,

            errorDialogTitle;

    public static String configsDoesntExist(Object text) {
        return format(configsDoesntExist, text);
    }

    static {
        NLS.initializeMessages("ru.petrovandrei.xoredentrytask.compositelaunch.shell.messages", Messages.class); //$NON-NLS-1$
    }
}