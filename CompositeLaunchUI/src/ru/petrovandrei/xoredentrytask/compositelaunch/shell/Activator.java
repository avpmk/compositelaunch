package ru.petrovandrei.xoredentrytask.compositelaunch.shell;

import java.net.MalformedURLException;
import java.net.URL;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import ru.petrovandrei.common.eclipse.log.ILog;
import ru.petrovandrei.common.eclipse.log.Log;

import static ru.petrovandrei.common.eclipse.launch.ConfigUtil.launchManager;
import static ru.petrovandrei.common.eclipse.log.NullILog.NULL_ILOG;
import static ru.petrovandrei.xoredentrytask.compositelaunch.RenamingListener.RENAMING_LISTENER;

public class Activator extends AbstractUIPlugin implements IStartup {

    private static final String ID = "ru.petrovandrei.xoredentrytask.compositeLaunchShell"; //$NON-NLS-1$
    private static final String PATH_ICONS = "icons/"; //$NON-NLS-1$

    public static ILog logger = NULL_ILOG;

    private static URL pluginInstallUrl;

    @SuppressWarnings("deprecation")
    @Override public void start(BundleContext context) throws Exception {
        super.start(context);
        pluginInstallUrl = getDescriptor().getInstallURL();
        logger = new Log(this, ID);
        launchManager().addLaunchConfigurationListener(RENAMING_LISTENER);
    }

    @Override public void stop(BundleContext context) throws Exception {
        launchManager().removeLaunchConfigurationListener(RENAMING_LISTENER);
        logger = NULL_ILOG;
        super.stop(context);
    }

    public static Image loadImage(String name) {
        try {
            return ImageDescriptor.createFromURL(new URL(pluginInstallUrl, PATH_ICONS + name)).createImage();
        } catch (MalformedURLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override public void earlyStartup() {}
}