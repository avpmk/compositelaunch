package ru.petrovandrei.xoredentrytask.compositelaunch.shell;

import org.eclipse.jface.dialogs.MessageDialog;
import ru.petrovandrei.xoredentrytask.compositelaunch.Messages;

import static ru.petrovandrei.xoredentrytask.compositelaunch.shell.Messages.errorDialogTitle;

public class Mb {

    private Mb() {}

    //todo + не блокирующий метод
    //todo + если повторно вызывается метод, а окно с сообщением всё ещё открыто не показывать следующее окно, а добавить в текущее
    public static void showMessage(String message) {
        MessageDialog.openInformation(null, Messages.pluginName, message);
    }

    private static void showErrorMessage(String message) {
        MessageDialog.openError(null, errorDialogTitle, message);
    }

    public static void showErrorMessage(String message, Exception exception) {
        showErrorMessage(message + "\n" + exception); //$NON-NLS-1$
    }

    public static void showErrorMessage(Exception exception) {
        showErrorMessage(exception.toString());
    }
}