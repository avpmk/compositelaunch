package ru.petrovandrei.xoredentrytask.compositelaunch.shell.ui;

import org.eclipse.debug.ui.AbstractLaunchConfigurationTabGroup;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;

import static ru.petrovandrei.common.util.ArrayUtil.asArray;

public class TabGroup extends AbstractLaunchConfigurationTabGroup {

    @Override
    public void createTabs(ILaunchConfigurationDialog dialog, String mode) {
        setTabs(asArray(new ConfigListTab()));
    }
}