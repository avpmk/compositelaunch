package ru.petrovandrei.xoredentrytask.compositelaunch.shell.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.debug.ui.IDebugModelPresentation;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.ISharedImages;
import ru.petrovandrei.common.eclipse.launch.UncheckedCoreException;
import ru.petrovandrei.common.util.IterableIterator;
import ru.petrovandrei.common.util.swt.TableWrp;
import ru.petrovandrei.xoredentrytask.compositelaunch.shell.Mb;
import ru.petrovandrei.xoredentrytask.compositelaunch.shell.Messages;
import ru.petrovandrei.xoredentrytask.compositelaunch.shell.contoller.ConfigsTreeController;
import ru.petrovandrei.xoredentrytask.compositelaunch.shell.model.CompositeConfigModel.CompositeConfigTreeOpertations;
import ru.petrovandrei.xoredentrytask.compositelaunch.shell.model.CompositeConfigModel;
import ru.petrovandrei.xoredentrytask.compositelaunch.shell.model.ConfigViewData;
import ru.petrovandrei.xoredentrytask.compositelaunch.shell.model.SinglyLinkedNode;
import ru.petrovandrei.xoredentrytask.compositelaunch.shell.model.TreeNodeActions;
import ru.petrovandrei.xoredentrytask.compositelaunch.shell.ui.util.MovableTreeItemMarker;

import static java.util.Arrays.stream;
import static java.util.Collections.addAll;
import static org.eclipse.debug.ui.DebugUITools.getLaunchGroup;
import static org.eclipse.debug.ui.DebugUITools.newDebugModelPresentation;
import static org.eclipse.debug.ui.DebugUITools.openLaunchConfigurationDialogOnGroup;
import static ru.petrovandrei.common.eclipse.launch.ConfigUtil.allConfigTypes;
import static ru.petrovandrei.common.eclipse.launch.ConfigUtil.configExists;
import static ru.petrovandrei.common.eclipse.launch.ConfigUtil.configTypesThatCanBeCreated;
import static ru.petrovandrei.common.util.swt.ButtonUtil.newCheckBox;
import static ru.petrovandrei.common.util.swt.ButtonUtil.newToolItem;
import static ru.petrovandrei.common.util.swt.Button_SwtFlag.DROP_DOWN;
import static ru.petrovandrei.common.util.swt.Button_SwtFlag.PUSH;
import static ru.petrovandrei.common.util.swt.ColumnLayoutUtil.createColumnLayout;
import static ru.petrovandrei.common.util.swt.GroupUtil.newGroupWithGridLayout;
import static ru.petrovandrei.common.util.swt.ImageUtil.sharedImage;
import static ru.petrovandrei.common.util.swt.MenuUtil.newMenuCascade;
import static ru.petrovandrei.common.util.swt.MenuUtil.newMenuItem;
import static ru.petrovandrei.common.util.swt.MenuUtil.newRadioMenuItem;
import static ru.petrovandrei.common.util.swt.fiadapters.ISelectionAdapter_widgetSelected.ws;
import static ru.petrovandrei.xoredentrytask.compositelaunch.shell.Activator.loadImage;
import static ru.petrovandrei.xoredentrytask.compositelaunch.shell.Activator.logger;
import static ru.petrovandrei.xoredentrytask.compositelaunch.shell.model.FiltationModeAllConfigs.INCLUDED_ONLY_TO_CURRENT;
import static ru.petrovandrei.xoredentrytask.compositelaunch.shell.model.FiltationModeAllConfigs.INCLUDED_TO_CURRENT_OR_ANY_SUBCONFIG;
import static ru.petrovandrei.xoredentrytask.compositelaunch.shell.model.FiltationModeCompositeConfigs.CONTAINS_CURRENT;
import static ru.petrovandrei.xoredentrytask.compositelaunch.shell.model.FiltationModeCompositeConfigs.CONTAINS_CURRENT_ANY_DEEP;
import static ru.petrovandrei.xoredentrytask.compositelaunch.shell.model.FiltationModeCompositeConfigs.NO_FILTRATION;

public class ConfigListTab extends AbstractLaunchConfigurationTab {

    //<editor-fold defaultstate="collapsed" desc="creation">
    private Composite root;

    @Override public void createControl(Composite parent) {
        root = new Composite(parent, SWT.NULL);
        root.setLayout(new GridLayout(3, false));
        root.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        setMessage(Messages.tabMessage);

        createPaneTreeConfigsToAdd();
        createToolBarButtonsAddRemove();
        createPaneTreeAndTableConfigsNested();

        setControl(root);
    }


    private Tree treeConfigsToAdd;

    private void createPaneTreeConfigsToAdd() {
        Group group = newGroupWithGridLayout(root, Messages.configsToAdd);

        Composite paneTreeConfigsToAdd = new Composite(group, SWT.NONE);
        treeConfigsToAdd = new Tree(paneTreeConfigsToAdd, SWT.MULTI);
        treeConfigsToAdd.addListener(SWT.MouseDoubleClick, e -> {
            //<editor-fold defaultstate="collapsed" desc="onDoubleClick">
            TreeItem item = treeConfigsToAdd.getItem(new Point(e.x, e.y));
            if (item != null && isMovableTreeItem(item)) {
                try {
                    model.add(item.getText());
                } catch (CoreException ex) {
                    logger.e(ex);
                    Mb.showErrorMessage(ex);
                }
                updateLaunchConfigurationDialog();
            }
            //</editor-fold>
        });
        treeConfigsToAdd.addListener(SWT.Selection, e -> onSelectionChangedTreeConfigsToAdd());

        paneTreeConfigsToAdd.setLayout(createColumnLayout(treeConfigsToAdd));
        paneTreeConfigsToAdd.setLayoutData(new GridData(GridData.FILL_BOTH));

        Menu menuFiltrate = createMenuFiltrate();
        ToolBar tbShowMenuFiltrate = new ToolBar(group, SWT.NONE);
        newToolItem(
                tbShowMenuFiltrate,
                DROP_DOWN,
                Messages.filtrationModes,
                loadImage("filter.png"), //$NON-NLS-1$
                e -> applyLocationAndShow(menuFiltrate, tbShowMenuFiltrate)
        );
    }

    private Menu createMenuFiltrate() {
        Menu menuFiltrate = new Menu(root);

        Menu menuFiltrateAll = newMenuCascade(menuFiltrate, Messages.filtrateAll);
        newRadioMenuItem(
                menuFiltrateAll,
                Messages.filtrateConfigsWhichIncludedToCurrent,
                e -> model.setFiltationModeAllConfigs(INCLUDED_ONLY_TO_CURRENT)
        ).setSelection(true);
        model.setFiltationModeAllConfigs(INCLUDED_ONLY_TO_CURRENT);

        newRadioMenuItem(
                menuFiltrateAll,
                Messages.filtrateConfigsWhichIncludedToCurrentOrAnySubconfig,
                e -> model.setFiltationModeAllConfigs(INCLUDED_TO_CURRENT_OR_ANY_SUBCONFIG)
        );

        Menu menuFiltrateComposite = newMenuCascade(menuFiltrate, Messages.filtrateComposite);
        newRadioMenuItem(
                menuFiltrateComposite,
                Messages.filtrateCompositeConfigsWhichContainsCurrent,
                e -> model.setFiltationModeCompositeConfigs(CONTAINS_CURRENT)
        );
        newRadioMenuItem(
                menuFiltrateComposite,
                Messages.filtrateCompositeConfigsWhichContainsCurrent_orAnySubconfigContainsCurrent,
                e -> model.setFiltationModeCompositeConfigs(CONTAINS_CURRENT_ANY_DEEP)
        );
        newRadioMenuItem(
                menuFiltrateComposite,
                Messages.dontFiltrateCompositeConfigs,
                e -> model.setFiltationModeCompositeConfigs(NO_FILTRATION)
        ).setSelection(true);
        model.setFiltationModeCompositeConfigs(NO_FILTRATION);

        return menuFiltrate;
    }


    private ToolItem btAdd, btRemove;

    private void createToolBarButtonsAddRemove() {
        ToolBar tbButtonsAddRemove = new ToolBar(root, SWT.VERTICAL);

        Image imageAdd = loadImage("arrow-forward.png"); //$NON-NLS-1$
        btAdd = newToolItem(tbButtonsAddRemove, PUSH, Messages.addSelectedConfigs, imageAdd, e -> addSelectedConfigs());
        btAdd.setEnabled(false);

        Image imageRemove = loadImage("arrow-back.png"); //$NON-NLS-1$
        btRemove = newToolItem(tbButtonsAddRemove, PUSH, Messages.removeSelectedConfigs, imageRemove, e -> removeSelectedConfigs());
        btRemove.setEnabled(false);
    }


    private Composite paneNestedConfigs;
    private Tree treeNestedConfigs;
    private TableWrp<ConfigViewData> tableNestedConfigs;
    private ToolItem btCreateNotExistingConfig;

    private void createPaneTreeAndTableConfigsNested() {
        Group groupConfigsNested = newGroupWithGridLayout(root, Messages.configsAdded);
        paneNestedConfigs = new Composite(groupConfigsNested, SWT.NONE);
        paneNestedConfigs.setLayoutData(new GridData(GridData.FILL_BOTH));

        showTreeNestedConfigs();

        Composite paneControl = new Composite(groupConfigsNested, SWT.NONE);

        GridLayout layout = new GridLayout(2, false);
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        layout.horizontalSpacing = 0;
        layout.verticalSpacing = 0;
        paneControl.setLayout(layout);
        paneControl.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        Button cbShowTree = newCheckBox(paneControl, Messages.showNestedConfigsHierarchicallyOrOnlyThatWillBeStarted, true);
        cbShowTree.addSelectionListener(ws(e -> {
            try {
                if (!cbShowTree.getSelection()) {
                    showTableNestedConfigs();
                    fillTableNestedConfigs();
                } else {
                    showTreeNestedConfigs();
                    fillTreeNestedConfigs();
                }
            } catch (CoreException ex) {
                logger.e(ex);
                Mb.showErrorMessage(ex);
            }
        }));

        ToolBar tbShowMenuCreate = new ToolBar(paneControl, SWT.NONE);

        GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
        gridData.horizontalAlignment = SWT.END;
        tbShowMenuCreate.setLayoutData(gridData);

        Menu menuCreateNotExistingConfig = createMenuCreateNotExistingConfig();
        btCreateNotExistingConfig = newToolItem(
                tbShowMenuCreate,
                DROP_DOWN,
                Messages.createNotExistedConfig,
                loadImage("add-document.png"), //$NON-NLS-1$
                e -> applyLocationAndShow(menuCreateNotExistingConfig, tbShowMenuCreate)
        );
        btCreateNotExistingConfig.setEnabled(false);
    }

    private Menu createMenuCreateNotExistingConfig() {
        Menu menuCreate = new Menu(root);
        IDebugModelPresentation mp = newDebugModelPresentation();
        String mode = getLaunchConfigurationDialog().getMode();
        configTypesThatCanBeCreated(mode).forEach(type -> {
            newMenuItem(
                    menuCreate,
                    type.getName(),
                    mp.getImage(type),
                    e -> createNotExistingSelectedConfig(type)
            );
        });
        return menuCreate;
    }
    //</editor-fold>

    private void showTreeNestedConfigs() {
        if (tableNestedConfigs != null) {
            //todo remember selection tableNestedConfigs?
            tableNestedConfigs.dispose();
            tableNestedConfigs = null;
        }

        treeNestedConfigs = new Tree(paneNestedConfigs, SWT.MULTI);
        treeNestedConfigs.addListener(SWT.Selection, e -> onSelectionChangedTreeNestedConfigs());
        paneNestedConfigs.setLayout(createColumnLayout(treeNestedConfigs));

        paneNestedConfigs.layout();
    }

    private void showTableNestedConfigs() {
        if (treeNestedConfigs != null) {
            rememberTreeNestedConfigsState();
            treeNestedConfigs.dispose();
            treeNestedConfigs = null;
        }

        tableNestedConfigs = new TableWrp<>(paneNestedConfigs);
        tableNestedConfigs.addSelectionListener(e -> onSelectionChangedTableNestedConfigs());
        paneNestedConfigs.setLayout(createColumnLayout(tableNestedConfigs.table));

        paneNestedConfigs.layout();
    }

    //<editor-fold defaultstate="collapsed" desc="actions">
    private void addSelectedConfigs() {
        try {
            for (TreeItem treeItem : treeConfigsToAdd.getSelection()) {
                model.add(treeItem.getText());
            }
            updateLaunchConfigurationDialog();
        } catch (CoreException ex) {
            logger.e(ex);
            Mb.showErrorMessage(ex);
        }
    }

    private void createNotExistingSelectedConfig(ILaunchConfigurationType type) {
        String configNameToCreate = (treeNestedConfigs != null)
                ? treeNestedConfigs.getSelection()[0].getText()
                : tableNestedConfigs.getFirstSelected().name;

        try {
            //todo убрать из view
            ILaunchConfiguration config = type.newInstance(null, configNameToCreate).doSave();
            openLaunchConfigurationDialogOnGroup(
                    getShell(), new StructuredSelection(config), getLaunchGroup(config, getLaunchConfigurationDialog().getMode()).getIdentifier()
            );
        } catch (CoreException ex) {
            logger.e(ex);
            Mb.showErrorMessage(ex);
        }
    }

    private void removeSelectedConfigs() {
        Stream<String> configNamesToRemove = (treeNestedConfigs != null)
                ? stream(treeNestedConfigs.getSelection()).map(e -> e.getText())
                : tableNestedConfigs.getSelectionData().map(e -> e.name);
        configNamesToRemove.forEach(model::remove);
        updateLaunchConfigurationDialog();
    }

    private void onSelectionChangedTreeConfigsToAdd() {
        btAdd.setEnabled(
                onlyMovableTreeItemsSelectedInTree(treeConfigsToAdd.getSelection())
        );
    }

    private void onSelectionChangedTreeNestedConfigs() {
        TreeItem[] selection = treeNestedConfigs.getSelection();

        btRemove.setEnabled(
                onlyMovableTreeItemsSelectedInTree(selection)
        );

        try {
            boolean canCreate = selection.length == 1 && !configExists(
                    selection[0].getText()
            );
            btCreateNotExistingConfig.setEnabled(canCreate);
        } catch (CoreException ex) {
            btCreateNotExistingConfig.setEnabled(false);
            Mb.showErrorMessage(ex);
        }
    }

    private void onSelectionChangedTableNestedConfigs() {
        TableItem[] selection = tableNestedConfigs.table.getSelection();

        btRemove.setEnabled(
                selection.length > 0
        );

        try {
            boolean canCreate = selection.length == 1 && !configExists(
                    ((ConfigViewData) selection[0].getData()).name
            );
            btCreateNotExistingConfig.setEnabled(canCreate);
        } catch (CoreException ex) {
            btCreateNotExistingConfig.setEnabled(false);
            Mb.showErrorMessage(ex);
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="util">
    private static void markAsMovableTreeItem(TreeItem treeItem) {
        treeItem.setData(MovableTreeItemMarker.MOVABLE_TREE_ITEM_MARKER);
    }

    private static boolean isMovableTreeItem(TreeItem treeItem) {
        return treeItem.getData() instanceof MovableTreeItemMarker;
    }

    private static boolean onlyMovableTreeItemsSelectedInTree(TreeItem[] selectedTreeItems) {
        for (TreeItem treeItem : selectedTreeItems) {
            if (!isMovableTreeItem(treeItem)) {
                return false;
            }
        }
        return selectedTreeItems.length > 0;
    }


    public static void applyLocationAndShow(Menu menu, Control showUnder) {
        Point point = showUnder.toDisplay(new Point(0, showUnder.getSize().y));
        menu.setLocation(point.x, point.y);
        menu.setVisible(true);
    }

    private SinglyLinkedNode<String> fullQualiefiedName(TreeItem treeItem) {
        final SinglyLinkedNode<String> leaf = new SinglyLinkedNode<>(treeItem.getText());
        SinglyLinkedNode<String> current = leaf;

        while ((treeItem = treeItem.getParentItem()) != null) {
            current = current.createParent(treeItem.getText());
        }

        current.createParent(model.getConfigName());
        return leaf;
    }

    private void rememberTreeState(Tree tree, Map<SinglyLinkedNode<String>, TreeViewState> mapStates) {
        mapStates.clear();

        //<editor-fold defaultstate="collapsed" desc="bfs">
        List<TreeItem>
                listActive        = new ArrayList<>(20),
                listToHandleLater = new ArrayList<>(20);

        addAll(listActive, tree.getItems());

        while (!listActive.isEmpty()) {
            //</editor-fold>
            for (TreeItem item : listActive) {
                if (item.getExpanded()) {
                    mapStates.put(fullQualiefiedName(item), TreeViewState.createExpanded());
                }
                addAll(listToHandleLater, item.getItems());
            }
            //<editor-fold defaultstate="collapsed" desc="bfs">
            listActive.clear();

            //<editor-fold defaultstate="collapsed" desc="swap listActive, listToHandleLater">
            List<TreeItem> tmpForSwap = listActive;
            listActive = listToHandleLater;
            listToHandleLater = tmpForSwap;
            //</editor-fold>
        }
        //</editor-fold>

        for (TreeItem item : tree.getSelection()) {
            SinglyLinkedNode<String> fullQualiefiedName = fullQualiefiedName(item);
            TreeViewState state = mapStates.get(fullQualiefiedName);
            if (state == null) {
                mapStates.put(fullQualiefiedName, state = new TreeViewState());
            }
            state.selected = true;
        }
    }
    //</editor-fold>

    private final CompositeConfigModel model = new CompositeConfigModel();

    @Override public void initializeFrom(ILaunchConfiguration configComposite) {
        try {
            model.setConfigComposite(configComposite);
            model.setConfigsTreeController(new ConfigsTreeController(this)); //todo не создавать каждый раз?
            updateLaunchConfigurationDialog();
        } catch (CoreException ex) {
            logger.e(ex);
            Mb.showErrorMessage(Messages.errorOnFormInitialization, ex);
        }
    }

    @Override protected void updateLaunchConfigurationDialog() {
        super.updateLaunchConfigurationDialog();
        try {
            CompositeConfigTreeOpertations treeOpertations = model.treeOpertations();
            IDebugModelPresentation mp = newDebugModelPresentation();

            updateTreeConfigsToAdd(treeOpertations, mp);

            if (treeNestedConfigs != null) {
                rememberTreeNestedConfigsState();
                treeNestedConfigs.removeAll();
                fillTreeNestedConfigs(treeOpertations, mp);
            } else {
                tableNestedConfigs.removeAll();
                fillTableNestedConfigs(treeOpertations, mp);
            }
        } catch (CoreException | UncheckedCoreException ex) {
            Mb.showErrorMessage(ex);
        }
    }

    public void updateTreeConfigsToAdd() throws CoreException {
        updateTreeConfigsToAdd(model.treeOpertations(), newDebugModelPresentation());
    }

    private void fillTableNestedConfigs() throws CoreException {
        fillTableNestedConfigs(model.treeOpertations(), newDebugModelPresentation());
    }

    private void fillTreeNestedConfigs() throws CoreException {
        fillTreeNestedConfigs(model.treeOpertations(), newDebugModelPresentation());
    }


    private static final TreeItem[] EMPTY_TREE_ITEMS_ARRAY = {};

    private final Set<String>
            namesExpandedConfigTypes        = new HashSet<>(),
            namesSelectedInTreeConfigsToAdd = new HashSet<>();

    private void updateTreeConfigsToAdd(CompositeConfigTreeOpertations treeOpertations, IDebugModelPresentation mp) throws CoreException {

        //<editor-fold defaultstate="collapsed" desc="remember expanded and selected names">
        namesExpandedConfigTypes.clear();
        for (TreeItem item : treeConfigsToAdd.getItems()) {
            if (item.getExpanded()) {
                namesExpandedConfigTypes.add(item.getText());
            }
        }

        namesSelectedInTreeConfigsToAdd.clear();
        for (TreeItem item : treeConfigsToAdd.getSelection()) {
            namesSelectedInTreeConfigsToAdd.add(item.getText());
        }
        //</editor-fold>

        treeConfigsToAdd.removeAll();

        List<TreeItem> newSelection = new ArrayList<>(
                namesSelectedInTreeConfigsToAdd.size()
        );

        for (ILaunchConfigurationType type : allConfigTypes()) {
            IterableIterator<ILaunchConfiguration> ii = treeOpertations.configsThatCanBeAddedToThisConfig(type); //todo val
            if (ii.hasNext()) {
                Image imageConfigType = mp.getImage(type);
                TreeItem tiConfigType = new TreeItem(treeConfigsToAdd, SWT.NONE);
                String typeName = type.getName();
                tiConfigType.setText(typeName);
                tiConfigType.setImage(imageConfigType);
                if (namesSelectedInTreeConfigsToAdd.contains(typeName)) {
                    newSelection.add(tiConfigType);
                }

                for (ILaunchConfiguration config : ii) {
                    TreeItem treeItem = new TreeItem(tiConfigType, SWT.NONE);
                    String configName = config.getName();
                    treeItem.setText(configName);
                    treeItem.setImage(imageConfigType);
                    markAsMovableTreeItem(treeItem);
                    if (namesSelectedInTreeConfigsToAdd.contains(configName)) {
                        newSelection.add(treeItem);
                    }
                }

                tiConfigType.setExpanded(namesExpandedConfigTypes.contains(typeName));
            }
        }

        treeConfigsToAdd.setSelection(
                newSelection.toArray(EMPTY_TREE_ITEMS_ARRAY)
        );
        onSelectionChangedTreeConfigsToAdd();
    }

    private static class ImagesHolder {
        static final Image
                IMAGE_ERROR = sharedImage(ISharedImages.IMG_OBJS_ERROR_TSK),
                IMAGE_RECURSIVE_COMPOSITE_CONFIG = loadImage("recursive-composite-config.png"); //$NON-NLS-1$
    }

    private void fillTableNestedConfigs(CompositeConfigTreeOpertations treeOpertations, IDebugModelPresentation mp) throws CoreException {
        List<String> namesNotExisting = new ArrayList<>();
        treeOpertations.forAllNestedConfigs_uniqNames((config, name) -> {
            Image image;
            if (config == null) {
                namesNotExisting.add(name);
                image = ImagesHolder.IMAGE_ERROR;
            } else {
                image = mp.getImage(config.getType());
            }
            tableNestedConfigs.add(new ConfigViewData(name, image));
        });

        onSelectionChangedTableNestedConfigs();
        setErrorMessage(namesNotExisting);
    }



    private final Map<SinglyLinkedNode<String>, TreeViewState> mapStatesTreeNestedConfigs = new HashMap<>();

    private void rememberTreeNestedConfigsState() {
        rememberTreeState(treeNestedConfigs, mapStatesTreeNestedConfigs);
    }

    private void fillTreeNestedConfigs(CompositeConfigTreeOpertations treeOpertations, IDebugModelPresentation mp) throws CoreException {
        Set<String> namesNotExisting = new HashSet<>();

        List<TreeItem>
                listNewSelection = new ArrayList<>(),
                listToExpand     = new ArrayList<>();

        treeOpertations.fillTree(
                () -> {
                    TreeItem ti = new TreeItem(treeNestedConfigs, SWT.NONE);
                    markAsMovableTreeItem(ti);
                    return ti;
                },
                new TreeNodeActions<TreeItem>() { //todo namesNotExisting, listNewSelection, listToExpand сделать полями, не создавать каждый раз TreeNodeActions

                    @Override
                    public TreeItem createChild(TreeItem treeItem) {
                        return new TreeItem(treeItem, SWT.NONE);
                    }

                    @Override
                    public void initialize(TreeItem treeItem, SinglyLinkedNode<String> fullQualifiedName) throws CoreException {
                        treeItem.setText(fullQualifiedName.value);
                        TreeViewState info = mapStatesTreeNestedConfigs.get(fullQualifiedName);
                        if (info != null) {
                            if (info.expanded) {
                                listToExpand.add(treeItem);
                            }
                            if (info.selected) {
                                listNewSelection.add(treeItem);
                            }
                        }
                    }

                    @Override
                    public void applyNormalState(TreeItem treeItem, ILaunchConfiguration config) throws CoreException {
                        treeItem.setImage(mp.getImage(config.getType()));
                    }

                    @Override
                    public void applyRemovedState(TreeItem treeItem, String name) {
                        namesNotExisting.add(name);
                        treeItem.setImage(ImagesHolder.IMAGE_ERROR);
                    }

                    @Override
                    public void applyRecursiveState(TreeItem treeItem) {
                        treeItem.setImage(ImagesHolder.IMAGE_RECURSIVE_COMPOSITE_CONFIG);
                    }
                }
        );

        for (TreeItem treeItemToExpand : listToExpand) {
            treeItemToExpand.setExpanded(true);
        }

        treeNestedConfigs.setSelection(
                listNewSelection.toArray(EMPTY_TREE_ITEMS_ARRAY)
        );

        onSelectionChangedTreeNestedConfigs();
        setErrorMessage(namesNotExisting);
    }

    protected void setErrorMessage(Collection<String> namesOfNotExistingConfigs) {
        super.setErrorMessage(namesOfNotExistingConfigs.isEmpty()
                ? null
                : Messages.configsDoesntExist(String.join(", ", namesOfNotExistingConfigs)) //$NON-NLS-1$
        );
    }

    @Override public void performApply(ILaunchConfigurationWorkingCopy configComposite) {
        try {
            model.saveTo(configComposite);
        } catch (CoreException ex) {
            Mb.showErrorMessage(Messages.failedToSave, ex);
        }
    }

    @Override public String getName() {
        return Messages.tabName;
    }

    @Override public void setDefaults(ILaunchConfigurationWorkingCopy config) {}
}