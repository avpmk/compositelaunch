package ru.petrovandrei.xoredentrytask.compositelaunch.shell.ui;

public class TreeViewState {

    public boolean selected, expanded;

    public static TreeViewState createExpanded() {
        TreeViewState treeViewState = new TreeViewState();
        treeViewState.expanded = true;
        return treeViewState;
    }
}