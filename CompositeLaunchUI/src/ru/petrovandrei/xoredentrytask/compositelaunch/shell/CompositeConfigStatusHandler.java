package ru.petrovandrei.xoredentrytask.compositelaunch.shell;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.core.IStatusHandler;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import ru.petrovandrei.xoredentrytask.compositelaunch.model.LaunchData;

import static org.eclipse.debug.ui.DebugUITools.getLaunchGroup;
import static org.eclipse.debug.ui.DebugUITools.openLaunchConfigurationDialogOnGroup;
import static org.eclipse.jface.dialogs.IDialogConstants.CANCEL_LABEL;
import static org.eclipse.jface.dialogs.IDialogConstants.OK_LABEL;
import static org.eclipse.jface.window.Window.OK;
import static ru.petrovandrei.common.util.ArrayUtil.asArray;

public class CompositeConfigStatusHandler implements IStatusHandler {

    @Override public Object handleStatus(IStatus status, Object source) {
        Shell activeShell = Display.getCurrent().getActiveShell();
        boolean sourceIsLaunchData = source instanceof LaunchData;

        //todo применить общий подход для вывода сообщений
        MessageDialog dialog = new MessageDialog( //todo val
                activeShell,
                Messages.errorDialogTitle,
                null, //dialogTitleImage
                status.getMessage(),
                mapIStatus_to_MessageDialogType(status),
                sourceIsLaunchData
                        ? asArray(Messages.editConfig, CANCEL_LABEL)
                        : asArray(OK_LABEL), //todo выпилить второй кейс?
                0
        );

        if (dialog.open() == OK && sourceIsLaunchData) {
            LaunchData data = (LaunchData) source;
            openLaunchConfigurationDialogOnGroup(
                    activeShell, new StructuredSelection(data.config), getLaunchGroup(data.config, data.mode).getIdentifier()
            );
        }

        return null;
    }

    private static int mapIStatus_to_MessageDialogType(IStatus status) {
        switch (status.getSeverity()) {
            case IStatus.ERROR:   return MessageDialog.ERROR;
            case IStatus.WARNING: return MessageDialog.WARNING;
            case IStatus.INFO:
            default:              return MessageDialog.INFORMATION;
        }
    }
}