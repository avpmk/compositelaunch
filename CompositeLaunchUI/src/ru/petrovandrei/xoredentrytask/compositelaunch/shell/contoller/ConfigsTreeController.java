package ru.petrovandrei.xoredentrytask.compositelaunch.shell.contoller;

import org.eclipse.core.runtime.CoreException;
import ru.petrovandrei.xoredentrytask.compositelaunch.shell.Mb;
import ru.petrovandrei.xoredentrytask.compositelaunch.shell.ui.ConfigListTab;

import static ru.petrovandrei.xoredentrytask.compositelaunch.shell.Activator.logger;

public class ConfigsTreeController implements IConfigsTreeController {

    private final ConfigListTab tab;

    public ConfigsTreeController(ConfigListTab tab) {
        this.tab = tab;
    }

    @Override
    public void updateTreeConfigsToAdd() {
        try {
            tab.updateTreeConfigsToAdd();
        } catch (CoreException ex) {
            logger.e(ex);
            Mb.showErrorMessage(ex);
        }
    }
}