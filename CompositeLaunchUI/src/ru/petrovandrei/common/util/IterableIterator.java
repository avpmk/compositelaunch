package ru.petrovandrei.common.util;

import java.util.Iterator;
import java.util.stream.BaseStream;

public class IterableIterator<T> implements Iterable<T>, Iterator<T> {

    private final Iterator<T> iterator;

    public IterableIterator(Iterator<T> iterator) {
        this.iterator = iterator;
    }

    public static <T, S extends BaseStream<T, S>> IterableIterator<T> asIterableIterator(BaseStream<T, S> stream) {
        return new IterableIterator<>(stream.iterator());
    }

    public static <T> IterableIterator<T> asIterableIterator(Iterable<T> iterable) {
        return new IterableIterator<>(iterable.iterator());
    }

    //<editor-fold defaultstate="collapsed" desc="Iterable impl">
    @Override public Iterator<T> iterator() {
        return iterator;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Iterator impl (delegation)">
    @Override public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override public T next() {
        return iterator.next();
    }

    @Override public void remove() {
        iterator.remove();
    }
    //</editor-fold>
}