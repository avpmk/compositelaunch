package ru.petrovandrei.common.util.swt.fiadapters;

import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;

public interface IKeyAdapter_keyReleased extends KeyListener {

    @Override public void keyReleased(KeyEvent e);
    @Override default public void keyPressed(KeyEvent e) {}

    /**
     * kp - keyReleased.
     * Метод для того, чтобы лаконично явно указать инстанс
     * какого функционального интерфейса нужен.
     */
    public static IKeyAdapter_keyReleased kp(IKeyAdapter_keyReleased toSpecify) {
        return toSpecify;
    }
}