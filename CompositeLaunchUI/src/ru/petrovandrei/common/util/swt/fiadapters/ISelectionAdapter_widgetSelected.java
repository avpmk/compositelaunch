package ru.petrovandrei.common.util.swt.fiadapters;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

public interface ISelectionAdapter_widgetSelected extends SelectionListener {

    @Override public void widgetSelected(SelectionEvent e);
    @Override default public void widgetDefaultSelected(SelectionEvent e) {}

    /**
     * ws - widgetSelected.
     * Метод для того, чтобы лаконично явно указать инстанс
     * какого функционального интерфейса нужен.
     */
    public static ISelectionAdapter_widgetSelected ws(ISelectionAdapter_widgetSelected toSpecify) {
        return toSpecify;
    }
}