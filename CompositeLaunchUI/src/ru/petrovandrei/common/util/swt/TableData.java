package ru.petrovandrei.common.util.swt;

import org.eclipse.swt.graphics.Image;

public interface TableData {
    String name();
    Image image();
}