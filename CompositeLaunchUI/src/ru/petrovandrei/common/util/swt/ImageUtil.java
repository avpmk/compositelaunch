package ru.petrovandrei.common.util.swt;

import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.PlatformUI;

public class ImageUtil {

    /** Get image from shared images. */
    public static Image sharedImage(String symbolicName) {
        return PlatformUI.getWorkbench().getSharedImages().getImage(symbolicName);
    }
}