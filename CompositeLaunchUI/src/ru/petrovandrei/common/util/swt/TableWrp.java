package ru.petrovandrei.common.util.swt;

import java.util.Arrays;
import java.util.stream.Stream;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import ru.petrovandrei.common.util.swt.fiadapters.ISelectionAdapter_widgetSelected;

/**
 * Обёртка над таблицей чтобы можно было использовать generic'и.
 * Наследованием воспользоваться было нельзя, т.к. фреймворк отказывался создавать унаследованный объект.
 */
//todo выпилить
public class TableWrp<T extends TableData> {

    public final Table table;

    public TableWrp(Composite parent) {
        this.table = new Table(parent, SWT.V_SCROLL);
    }

    public void add(T e) {
        TableItem item = new TableItem(table, SWT.NONE);
        item.setText(0, e.name());
        item.setImage(e.image());
        item.setData(e);
    }

    @SuppressWarnings("unchecked")
    public Stream<T> getSelectionData() {
        return Arrays.stream(table.getSelection()).map(ti -> (T) ti.getData());
    }

    @SuppressWarnings("unchecked")
    public T getFirstSelected() {
        TableItem[] selection = table.getSelection();
        if (selection.length > 0) {
            return (T) selection[0].getData();
        } else {
            return null;
        }
    }

    //<editor-fold defaultstate="collapsed" desc="delegates">
    public void addSelectionListener(ISelectionAdapter_widgetSelected onWidgetSelected) { table.addSelectionListener(onWidgetSelected); }
    public void removeAll() { table.removeAll(); }
    public void dispose() { table.dispose(); }
    //</editor-fold>
}