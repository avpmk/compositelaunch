package ru.petrovandrei.common.util.swt;

import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.layout.TreeColumnLayout;
import org.eclipse.jface.viewers.ColumnPixelData;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;

public class ColumnLayoutUtil {

    //todo поробовать убрать абсолютные размеры
    private static final int COLUMN_WIDTH = 200;

    public static TreeColumnLayout createColumnLayout(Tree tree) {
        TreeColumnLayout layout = new TreeColumnLayout();
        layout.setColumnData(
                new TreeColumn(tree, SWT.LEFT),
                new ColumnPixelData(COLUMN_WIDTH)
        );
        return layout;
    }

    public static TableColumnLayout createColumnLayout(Table table) {
        TableColumnLayout layout = new TableColumnLayout();
        layout.setColumnData(
                new TableColumn(table, SWT.LEFT),
                new ColumnPixelData(COLUMN_WIDTH)
        );
        return layout;
    }
}