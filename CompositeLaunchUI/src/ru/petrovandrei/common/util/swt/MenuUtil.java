package ru.petrovandrei.common.util.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import ru.petrovandrei.common.util.swt.fiadapters.ISelectionAdapter_widgetSelected;

import static ru.petrovandrei.common.util.swt.fiadapters.ISelectionAdapter_widgetSelected.ws;

public class MenuUtil {

    public static MenuItem newMenuItem(Menu menuParent, String text, Image image, ISelectionAdapter_widgetSelected onAction) {
        MenuItem menuItem = new MenuItem(menuParent, SWT.PUSH);
        menuItem.setText(text);
        menuItem.setImage(image);
        menuItem.addSelectionListener(onAction);
        return menuItem;
    }

    public static MenuItem newRadioMenuItem(Menu menuParent, String text, ISelectionAdapter_widgetSelected onSelected) {
        MenuItem menuItem = new MenuItem(menuParent, SWT.RADIO);
        menuItem.setText(text);
        menuItem.addSelectionListener(ws(e -> {
            if (((MenuItem) e.getSource()).getSelection()) {
                onSelected.widgetSelected(e);
            }
        }));
        return menuItem;
    }

    public static Menu newMenuCascade(Menu menuParent, String text) {
        MenuItem menuItem = new MenuItem(menuParent, SWT.CASCADE);
        menuItem.setText(text);
//        menuItem.setImage(image);
        Menu menuForChildren = new Menu(menuParent);
        menuItem.setMenu(menuForChildren);
        return menuForChildren;
    }
}