package ru.petrovandrei.common.util.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import ru.petrovandrei.common.util.swt.fiadapters.ISelectionAdapter_widgetSelected;

public class ButtonUtil {

    public static ToolItem newToolItem(ToolBar parent, Button_SwtFlag style, String text, Image image, ISelectionAdapter_widgetSelected onAction) {
        ToolItem button = new ToolItem(parent, style.value);
        button.setToolTipText(text);
        button.setImage(image);
        button.addSelectionListener(onAction);
        return button;
    }

    public static Button newCheckBox(Composite parent, String text, boolean selected) {
        Button button = new Button(parent, SWT.CHECK);
        button.setText(text);
        button.setSelection(selected);
        return button;
    }
}