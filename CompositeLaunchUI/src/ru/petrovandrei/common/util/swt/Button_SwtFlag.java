package ru.petrovandrei.common.util.swt;

import org.eclipse.swt.SWT;

/** A little bit more typesafe. */
public enum Button_SwtFlag {

    DROP_DOWN(
            SWT.DROP_DOWN
    ),
    PUSH(
            SWT.PUSH
    );

    public final int value;

    private Button_SwtFlag(int value) {
        this.value = value;
    }
}