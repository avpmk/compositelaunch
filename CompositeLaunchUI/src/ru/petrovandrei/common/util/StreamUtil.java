package ru.petrovandrei.common.util;

import java.util.stream.BaseStream;

public class StreamUtil {

    /** Отложенный вызов итератора. */
    //todo mark as _delayed?
    public static <T, S extends BaseStream<T, S>> Iterable<T> streamAsIterable(BaseStream<T, S> stream) {
        return () -> stream.iterator();
    }
}