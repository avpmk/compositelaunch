package ru.petrovandrei.common.fi;

public interface ConsumerThr<T, H extends Throwable> {
    void accept(T t) throws H;
}