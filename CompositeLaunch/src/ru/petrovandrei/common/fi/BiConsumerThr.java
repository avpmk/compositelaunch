package ru.petrovandrei.common.fi;

public interface BiConsumerThr<T, U, H extends Throwable> {
    void accept(T t, U u) throws H;
}