package ru.petrovandrei.common.fi;

public interface BiPredicateThr<T, U, H extends Throwable> {
    boolean test(T t, U u) throws H;
}