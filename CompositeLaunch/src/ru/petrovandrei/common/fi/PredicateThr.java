package ru.petrovandrei.common.fi;

public interface PredicateThr<S, H extends Throwable> {
    boolean test(S src) throws H;
}