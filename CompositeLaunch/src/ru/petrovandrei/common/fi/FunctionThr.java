package ru.petrovandrei.common.fi;

/**
 * Нужно явно указывать сигнатуру интерфейса иначе компилятор вылетает с NPE.
 *
 * http://sql.ru/forum/1119573/
 */
public interface FunctionThr<S, R, H extends Throwable> {
    R apply(S src) throws H;
}