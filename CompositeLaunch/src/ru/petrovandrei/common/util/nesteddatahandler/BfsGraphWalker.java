package ru.petrovandrei.common.util.nesteddatahandler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import ru.petrovandrei.common.fi.BiConsumerThr;
import ru.petrovandrei.common.fi.FunctionThr;
import ru.petrovandrei.common.fi.PredicateThr;

/**
 * Для параметра provider нужно явно указывать сигнатуру интерфейса, иначе компилятор вылетает с NPE.
 *
 * @param <S> source (and result) type
 * @param <I> intermediate type
 */
public class BfsGraphWalker<S, I, H extends Throwable> {

    private final FunctionThr<S, ? extends Collection<I>, H> provider;
    private final Function<I, S> mapper;
    private final PredicateThr<S, H> isCompositeNode;

    /**
     * @param provider функциональный интрефейс, вход - узел S, выход - коллекция узлов I (которые mapper превращает в узлы S)
     */
    public BfsGraphWalker(FunctionThr<S, ? extends Collection<I>, H> provider, Function<I, S> mapper, PredicateThr<S, H> isCompositeNode) {
        this.provider = provider;
        this.mapper = mapper;
        this.isCompositeNode = isCompositeNode;
    }

    /**
     * Не заходит второй раз в узлы I
     * @param resultsHandler Обработчик для конечных элементов (и I элементов из которых они были получены)
     */
    public void foreachLeaf(S sourceRoot, BiConsumerThr<S, I, H> resultsHandler) throws H {
        foreachLeaf(provider.apply(sourceRoot), resultsHandler);
    }

    /**
     * Не заходит второй раз в узлы I
     * @param resultsHandler Обработчик для конечных элементов (и I элементов из которых они были получены)
     */
    public void foreachLeaf(Collection<I> roots, BiConsumerThr<S, I, H> resultsHandler) throws H {
        Set<I> set = new HashSet<>(200);
        //<editor-fold defaultstate="collapsed" desc="copy-paste">
        ArrayList<I>
                listActive        = new ArrayList<>(100),
                listToHandleLater = new ArrayList<>(100);

        listActive.addAll(roots);

        while (!listActive.isEmpty()) {
        //</editor-fold>
            for (I intermediate : listActive) {
                if (set.add(intermediate)) {
                    S source = mapper.apply(intermediate);
                    if (isCompositeNode.test(source)) {
                        listToHandleLater.addAll(provider.apply(source));
                    } else {
                        resultsHandler.accept(source, intermediate);
                    }
                }
            }
        //<editor-fold defaultstate="collapsed" desc="copy-paste">

            listActive.clear();

            //<editor-fold defaultstate="collapsed" desc="swap listActive, listToHandleLater">
            ArrayList<I> tmpForSwap = listActive;
            listActive = listToHandleLater;
            listToHandleLater = tmpForSwap;
            //</editor-fold>
        }
        //</editor-fold>
    }

    /** Не заходит второй раз в узлы I. */
    public boolean matchAny(S sourceRoot, Predicate<I> matchCondition) throws H {
        return matchAny(provider.apply(sourceRoot), matchCondition);
    }

    /** Не заходит второй раз в узлы I. */
    public boolean matchAny(Collection<I> roots, Predicate<I> matchCondition) throws H {
        Set<I> set = new HashSet<>(200);
        //<editor-fold defaultstate="collapsed" desc="copy-paste">
        ArrayList<I>
                listActive        = new ArrayList<>(100),
                listToHandleLater = new ArrayList<>(100);

        listActive.addAll(roots);

        while (!listActive.isEmpty()) {
        //</editor-fold>
            for (I intermediate : listActive) {
                if (set.add(intermediate)) {
                    if (matchCondition.test(intermediate)) {
                        return true;
                    }
                    S source = mapper.apply(intermediate);
                    if (isCompositeNode.test(source)) {
                        listToHandleLater.addAll(provider.apply(source));
                    }
                }
            }
        //<editor-fold defaultstate="collapsed" desc="copy-paste">

            listActive.clear();

            //<editor-fold defaultstate="collapsed" desc="swap listActive, listToHandleLater">
            ArrayList<I> tmpForSwap = listActive;
            listActive = listToHandleLater;
            listToHandleLater = tmpForSwap;
            //</editor-fold>
        }
        //</editor-fold>
        return false;
    }
}