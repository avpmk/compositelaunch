package ru.petrovandrei.common.eclipse.log;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import ru.petrovandrei.common.eclipse.launch.UncheckedCoreException;

public interface ILog {

    void e(String message, Exception exception);
    void log(Exception exception);
    void e(String message);

    void e(CoreException coreException);
    void e(UncheckedCoreException coreException);
    void log(IStatus status);
}