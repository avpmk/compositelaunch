package ru.petrovandrei.common.eclipse.log;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import ru.petrovandrei.common.eclipse.launch.UncheckedCoreException;

import static org.eclipse.core.runtime.IStatus.ERROR;

public class Log implements ILog {

    private final Plugin plugin;
    private final String pluginId;

    public Log(Plugin plugin, String pluginId) {
        this.plugin = plugin;
        this.pluginId = pluginId;
    }

    @Override public void e(CoreException coreException) {
        log(coreException.getStatus());
    }

    @Override public void e(UncheckedCoreException uncheckedCoreException) {
        log(uncheckedCoreException.coreException.getStatus());
    }

    @Override public void e(String message) {
        log(new Status(ERROR, pluginId, message));
    }

    @Override public void log(IStatus status) {
        plugin.getLog().log(status);
    }

    @Override public void e(String message, Exception exception) {
        log(new Status(ERROR, pluginId, message, exception));
    }

    @Override public void log(Exception exception) {
        e(exception.toString(), exception); //exception.toString() т.к. попадались exceptions c пустыми свойствами message и localizedMessage
    }
}