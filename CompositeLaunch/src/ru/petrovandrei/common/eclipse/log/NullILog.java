package ru.petrovandrei.common.eclipse.log;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import ru.petrovandrei.common.eclipse.launch.UncheckedCoreException;

//todo может не делать синглтоном..
public class NullILog implements ILog {

    public static final NullILog NULL_ILOG = new NullILog();

    private NullILog() {}

    @Override public void e(CoreException coreException) {}
    @Override public void e(UncheckedCoreException uncheckedCoreException) {}
    @Override public void e(String message) {}
    @Override public void log(IStatus status) {}
    @Override public void log(Exception exception) {}
    @Override public void e(String message, Exception exception) {}
}