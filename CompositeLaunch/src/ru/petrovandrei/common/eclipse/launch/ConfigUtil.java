package ru.petrovandrei.common.eclipse.launch;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchManager;
import ru.petrovandrei.common.collections.readonlymap.ReadOnlyMap;

import static java.util.Arrays.stream;
import static java.util.Comparator.comparing;
import static ru.petrovandrei.common.collections.readonlymap.ReadOnlyMaps.newReadOnlyMap_basedOnHashMap_fromValues;

public class ConfigUtil {

    private ConfigUtil() {}

    public static ILaunchManager launchManager() {
        return DebugPlugin.getDefault().getLaunchManager();
    }

    /** O(n) */
    public static boolean configExists(String name) throws CoreException {
        for (ILaunchConfiguration config : launchManager().getLaunchConfigurations()) {
            if (name.equals(config.getName())) {
                return true;
            }
        }
        return false;
    }

    //todo rename to createMapOfConfigs?
    /** Short-circuit read-only object. Short-circuit поскольку коллекция не поддерживается в актуальном состоянии. */
    public static ReadOnlyMap<String, ILaunchConfiguration> mapOfLaunchConfigurations() throws CoreException {
        return newReadOnlyMap_basedOnHashMap_fromValues(
                launchManager().getLaunchConfigurations(), v -> v.getName()
        );
    }

    public static ILaunchConfigurationType[] allConfigTypes() {
        return launchManager().getLaunchConfigurationTypes();
    }

    public static Stream<ILaunchConfigurationType> configTypesThatCanBeCreated(String mode) {
        return stream(allConfigTypes())
                .filter(type -> type.supportsMode(mode) && type.isPublic() && type.getCategory() == null)
                .sorted(comparing(type -> type.getName()));
    }

    /** если передать null - вернёт пустой массив */
    public static ILaunchConfiguration[] configs(ILaunchConfigurationType type) throws CoreException {
        return launchManager().getLaunchConfigurations(type);
    }

    public static ILaunchConfiguration[] configsById(String typeId) throws CoreException, LaunchConfigurationTypeNotFoundException {
        ILaunchConfigurationType type = launchManager().getLaunchConfigurationType(typeId);
        if (type == null) {
            throw new LaunchConfigurationTypeNotFoundException();
        }

        return configs(type);
    }
}