package ru.petrovandrei.common.eclipse.launch;

import org.eclipse.core.runtime.CoreException;

public class UncheckedCoreException extends RuntimeException {

    static final long serialVersionUID = 1;

    public final CoreException coreException;

    public UncheckedCoreException(CoreException coreException) {
        this.coreException = coreException;
    }

    @Override public String toString() {
        return coreException.toString();
    }
}