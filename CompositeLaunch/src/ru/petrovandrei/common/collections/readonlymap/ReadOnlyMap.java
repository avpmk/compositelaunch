package ru.petrovandrei.common.collections.readonlymap;

/**
 * Определяет только метод чтения из Map'ы.
 * Может быть заимплементирован, в т.ч. на Map'е, которую можно модифицировать
 * (-> возможно не очень удачное название для интерфейса).
 */
public interface ReadOnlyMap<K, V> {
    boolean containsKey(Object key);
    V get(K key);
}