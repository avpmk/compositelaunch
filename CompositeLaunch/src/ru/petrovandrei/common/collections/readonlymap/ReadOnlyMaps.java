package ru.petrovandrei.common.collections.readonlymap;

import java.util.function.Function;

public class ReadOnlyMaps {

    private ReadOnlyMaps() {}

    public static <K, V> ReadOnlyMap<K, V> newReadOnlyMap_basedOnHashMap(K[] keys, Function<K, V> mapperToMakeValue) {
        ModifiableHashMap<K, V> map = new ModifiableHashMap<>(keys.length, 1f); //todo val
        for (K key : keys) {
            map.put(
                    key, mapperToMakeValue.apply(key)
            );
        }
        return map;
    }

    public static <K, V> ReadOnlyMap<K, V> newReadOnlyMap_basedOnHashMap_fromValues(V[] values, Function<V, K> mapperToMakeKey) {
        ModifiableHashMap<K, V> map = new ModifiableHashMap<>(values.length, 1f); //todo val
        for (V value : values) {
            map.put(
                    mapperToMakeKey.apply(value), value
            );
        }
        return map;
    }
}