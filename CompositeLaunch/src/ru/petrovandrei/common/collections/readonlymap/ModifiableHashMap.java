package ru.petrovandrei.common.collections.readonlymap;

import java.util.HashMap;
import java.util.Map;

/**
 * Обычный HashMap. Без переопределеных методов.
 * Вызов модифицирующих методов не приведёт к exception'у.
 * Но реализующий {@link ReadOnlyMap}.
 */
public class ModifiableHashMap<K, V> extends HashMap<K, V> implements ReadOnlyMap<K, V> {

    //<editor-fold defaultstate="collapsed" desc="конструкторы HashMap">
    public ModifiableHashMap() {}
    public ModifiableHashMap(Map<? extends K, ? extends V> m) { super(m); }
    public ModifiableHashMap(int initialCapacity) { super(initialCapacity); }
    public ModifiableHashMap(int initialCapacity, float loadFactor) { super(initialCapacity, loadFactor); }
    //</editor-fold>
}