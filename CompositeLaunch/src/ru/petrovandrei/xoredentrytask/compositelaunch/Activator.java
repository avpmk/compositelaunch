package ru.petrovandrei.xoredentrytask.compositelaunch;

import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.osgi.framework.BundleContext;
import ru.petrovandrei.common.eclipse.log.ILog;
import ru.petrovandrei.common.eclipse.log.Log;

import static org.eclipse.core.runtime.IStatus.ERROR;
import static ru.petrovandrei.common.eclipse.launch.ConfigUtil.launchManager;
import static ru.petrovandrei.common.eclipse.log.NullILog.NULL_ILOG;
import static ru.petrovandrei.xoredentrytask.compositelaunch.RenamingListener.RENAMING_LISTENER;

public class Activator extends Plugin {

    private static final String ID = "ru.petrovandrei.xoredentrytask.compositeLaunch"; //$NON-NLS-1$

    public static ILog logger = NULL_ILOG;

    public static Status newStatusError(String message) {
        return new Status(ERROR, ID, message);
    }

    public static Status newStatusError(String message, Exception e) {
        return new Status(ERROR, ID, message, e);
    }

    @Override public void start(BundleContext context) throws Exception {
        super.start(context);
        launchManager().addLaunchConfigurationListener(RENAMING_LISTENER);
        logger = new Log(this, ID);
    }

    @Override public void stop(BundleContext context) throws Exception {
        logger = NULL_ILOG;
        launchManager().removeLaunchConfigurationListener(RENAMING_LISTENER);
        super.stop(context);
    }
}