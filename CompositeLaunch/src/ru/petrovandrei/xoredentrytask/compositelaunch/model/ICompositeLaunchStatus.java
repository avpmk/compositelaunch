package ru.petrovandrei.xoredentrytask.compositelaunch.model;

public interface ICompositeLaunchStatus {
    String message();
}