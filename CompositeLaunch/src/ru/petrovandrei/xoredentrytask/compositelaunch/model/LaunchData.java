package ru.petrovandrei.xoredentrytask.compositelaunch.model;

import org.eclipse.debug.core.ILaunchConfiguration;

//todo @Data
public class LaunchData {

    public final ILaunchConfiguration config;
    public final String mode;

    public LaunchData(ILaunchConfiguration config, String mode) {
        this.config = config;
        this.mode = mode;
    }
}