package ru.petrovandrei.xoredentrytask.compositelaunch.model;

import java.io.IOException;
import java.util.Collection;
import java.util.Properties;

import static ru.petrovandrei.xoredentrytask.compositelaunch.Activator.logger;

public enum CompositeLaunchStatus implements ICompositeLaunchStatus {
    OK,
    EMPTY,
    HAS_NOT_EXISTING_CONFIG_NAMES,
    ALL_CONFIG_NAMES_DOES_NOT_EXIST;

    private static final int LAST_INDEX = 3; //количество статусов - 1
    private static final int EXPECTED_AVG_NAME_LENGTH = 100; //не слишком большая, но достаточная чтобы избежать перевыделений массива в sb

    public static ICompositeLaunchStatus checkStatus(boolean hasGood, Collection<String> notExistingConfigNames) {
        if (hasGood) {
            if (notExistingConfigNames.isEmpty()) {
                return OK;
            }
            //<editor-fold defaultstate="collapsed" desc="sbWrongConfigNames">
            StringBuilder sbWrongConfigNames = new StringBuilder(notExistingConfigNames.size() * EXPECTED_AVG_NAME_LENGTH)
                    .append(HAS_NOT_EXISTING_CONFIG_NAMES.message)
                    .append(":");//$NON-NLS-1$

            for (String name : notExistingConfigNames) {
                sbWrongConfigNames.append("\n").append(name);//$NON-NLS-1$
            }
            //</editor-fold>
            String msg = sbWrongConfigNames.toString();
            return () -> msg;
        } else {
            return notExistingConfigNames.isEmpty() ? EMPTY : ALL_CONFIG_NAMES_DOES_NOT_EXIST;
        }
    }

    @Override public String message() {
        return message;
    }

    private final String message;

    private static class PropertiesHolder {
        static Properties p;
    }

    private CompositeLaunchStatus() {
        String name = name();
        switch (ordinal()) {
            case 0:
                PropertiesHolder.p = new Properties();
                try {
                    Class<?> cls = CompositeLaunchStatus.class;
                    PropertiesHolder.p.load(cls.getResourceAsStream("CompositeLaunchStatus.properties")); //$NON-NLS-1$
                } catch (IOException ex) {
                    logger.log(ex);
                }
            default:
                message = PropertiesHolder.p.getProperty(name, name);
            break; //не буду больше так писать, читается плохо
            case LAST_INDEX:
                message = PropertiesHolder.p.getProperty(name, name);
                PropertiesHolder.p = null;
        }
    }
}