package ru.petrovandrei.xoredentrytask.compositelaunch.util;

import java.util.Collection;
import java.util.function.Function;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import ru.petrovandrei.common.fi.FunctionThr;
import ru.petrovandrei.common.fi.PredicateThr;
import ru.petrovandrei.common.util.nesteddatahandler.BfsGraphWalker;

/** Для того чтобы в коде не писать generic параметры. */
public class ConfigsTreeWalker extends BfsGraphWalker<ILaunchConfiguration, String, CoreException> {

    public ConfigsTreeWalker(FunctionThr<ILaunchConfiguration, ? extends Collection<String>, CoreException> provider, Function<String, ILaunchConfiguration> mapper, PredicateThr<ILaunchConfiguration, CoreException> needCallProvider) {
        super(provider, mapper, needCallProvider);
    }
}