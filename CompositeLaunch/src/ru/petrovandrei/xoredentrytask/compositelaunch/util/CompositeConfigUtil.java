package ru.petrovandrei.xoredentrytask.compositelaunch.util;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import ru.petrovandrei.common.eclipse.launch.ConfigUtil;
import ru.petrovandrei.common.eclipse.launch.LaunchConfigurationTypeNotFoundException;

import static java.util.Collections.replaceAll;
import static ru.petrovandrei.common.eclipse.launch.ConfigUtil.mapOfLaunchConfigurations;

public class CompositeConfigUtil {

    private CompositeConfigUtil() {}

    private static final String COMPOSITE_TYPE_ID = "ru.petrovandrei.xoredentrytask.compositeLaunch"; //$NON-NLS-1$

    public static void replaceNestedConfigNames(String oldName, String newName) throws CoreException, LaunchConfigurationTypeNotFoundException {
        for (ILaunchConfiguration configComposite : ConfigUtil.configsById(COMPOSITE_TYPE_ID)) {
            List<String> names = getNestedConfigNames(configComposite);
            if (replaceAll(names, oldName, newName)) {
                setNestedConfigNames_inWorkingCopy(configComposite, names);
            }
        }
    }

    public static boolean isComposite(ILaunchConfiguration config) throws CoreException {
        return config != null && COMPOSITE_TYPE_ID.equals(config.getType().getIdentifier());
    }

    //<editor-fold defaultstate="collapsed" desc="NestedConfigNames property">
    private final static String LAUNCH_CONFIGS_LIST_ID = "ru.petrovandrei.xoredentrytask.launchConfigNames"; //$NON-NLS-1$

    /** Изменения этой коллекции не приведут к изменениям в рабочей копии. */
    public static List<String> getNestedConfigNames(ILaunchConfiguration configComposite) throws CoreException {
        return configComposite.getAttribute(LAUNCH_CONFIGS_LIST_ID, Collections.EMPTY_LIST);
    }

    public static void setNestedConfigNames(ILaunchConfigurationWorkingCopy configComposite, List<String> names) {
        configComposite.setAttribute(LAUNCH_CONFIGS_LIST_ID, names);
    }

    public static void setNestedConfigNames_inWorkingCopy(ILaunchConfiguration configComposite, List<String> names) throws CoreException {
        configComposite.getWorkingCopy().setAttribute(LAUNCH_CONFIGS_LIST_ID, names);
    }
    //</editor-fold>

    public static ConfigsTreeWalker configsTree(Function<String, ILaunchConfiguration> mapper) throws CoreException {
        return new ConfigsTreeWalker(
                c -> getNestedConfigNames(c),
                mapper,
                c -> isComposite(c)
        );
    }

    public static ConfigsTreeWalker configsTree() throws CoreException {
        return configsTree(mapOfLaunchConfigurations()::get);
    }
}