package ru.petrovandrei.xoredentrytask.compositelaunch;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.IStatusHandler;
import org.eclipse.debug.core.model.LaunchConfigurationDelegate;
import ru.petrovandrei.xoredentrytask.compositelaunch.model.ICompositeLaunchStatus;
import ru.petrovandrei.xoredentrytask.compositelaunch.model.LaunchData;

import static ru.petrovandrei.xoredentrytask.compositelaunch.Activator.logger;
import static ru.petrovandrei.xoredentrytask.compositelaunch.Activator.newStatusError;
import static ru.petrovandrei.xoredentrytask.compositelaunch.model.CompositeLaunchStatus.OK;
import static ru.petrovandrei.xoredentrytask.compositelaunch.model.CompositeLaunchStatus.checkStatus;
import static ru.petrovandrei.xoredentrytask.compositelaunch.util.CompositeConfigUtil.configsTree;

public class LaunchDelegate extends LaunchConfigurationDelegate {

    @Override public void launch(ILaunchConfiguration configComposite, String mode, ILaunch launch, IProgressMonitor monitor) throws CoreException {
        List<ILaunchConfiguration> configsToStart = getAllNestedConfigs(configComposite);
        CountDownLatch countDownLatch = new CountDownLatch(configsToStart.size()); //todo cyclicBarrier instead?
        configsToStart.stream().forEach(config -> launchAsync(mode, config, countDownLatch));

        try {
            countDownLatch.await();
        } catch (InterruptedException ie) {
            throw new CoreException(newStatusError(
                    Messages.errorOnWaitingForNestedConfigsComplete, ie
            ));
        }
    }

    private static void launchAsync(String mode, ILaunchConfiguration config, CountDownLatch countDownLatch) {
        Job job = new Job(Messages.launch) {
            @Override protected IStatus run(IProgressMonitor monitor) {
                try {
                    config.launch(mode, monitor, true, true);
                    return Status.OK_STATUS;
                } catch (CoreException e) {
                    return e.getStatus(); //todo log?
                } finally {
                    countDownLatch.countDown();
                }
            }
        };
        job.setPriority(Job.INTERACTIVE);
        job.schedule();
    }

    @Override public boolean preLaunchCheck(ILaunchConfiguration configComposite, String mode, IProgressMonitor monitor) throws CoreException {
        List<String> wrongNames = new ArrayList<>(50);
        boolean[] hasGood = {false};

        configsTree().foreachLeaf(configComposite,
                (config, name) -> {
                    if (config == null) {
                        wrongNames.add(name);
                    } else {
                        hasGood[0] = true;
                    }
                }
        );

        ICompositeLaunchStatus launchStatus = checkStatus(hasGood[0], wrongNames);
        boolean isOk = launchStatus == OK;
        if (!isOk) {
            Status status = newStatusError(launchStatus.message());
            IStatusHandler statusHandler = DebugPlugin.getDefault().getStatusHandler(promptStatus);
            if (statusHandler != null) {
                try {
                    statusHandler.handleStatus(status, new LaunchData(configComposite, mode));
                } catch (CoreException cause) {
                    Status statusWithException = newStatusError(launchStatus.message(), cause);
                    logger.log(statusWithException);
                    throw new CoreException(statusWithException);
                }
            } else {
                logger.log(status);
                throw new CoreException(status);
            }
        }
        return isOk;
    }

    //todo move to CompositeConfigUtil?
    public static List<ILaunchConfiguration> getAllNestedConfigs(ILaunchConfiguration configComposite) throws CoreException {
        List<String> notExistingConfigNames = new ArrayList<>();
        List<ILaunchConfiguration> configs = new ArrayList<>();

        configsTree().foreachLeaf(configComposite,
                (config, name) -> {
                    if (config == null) {
                        notExistingConfigNames.add(name);
                    } else {
                        configs.add(config);
                    }
                }
        );

        ICompositeLaunchStatus status = checkStatus(!configs.isEmpty(), notExistingConfigNames);
        if (status != OK) {
            throw new CoreException(newStatusError(
                    status.message()
            ));
        }

        return configs;
    }
}