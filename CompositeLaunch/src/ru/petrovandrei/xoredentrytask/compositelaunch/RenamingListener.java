package ru.petrovandrei.xoredentrytask.compositelaunch;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationListener;
import ru.petrovandrei.common.eclipse.launch.LaunchConfigurationTypeNotFoundException;

import static ru.petrovandrei.common.eclipse.launch.ConfigUtil.launchManager;
import static ru.petrovandrei.xoredentrytask.compositelaunch.util.CompositeConfigUtil.replaceNestedConfigNames;

public class RenamingListener implements ILaunchConfigurationListener {

    public static final RenamingListener RENAMING_LISTENER = new RenamingListener();

    private RenamingListener() {}

    @Override public void launchConfigurationAdded(ILaunchConfiguration configNew) {
        ILaunchConfiguration configOld = launchManager().getMovedFrom(configNew);
        if (configOld != null) {
            String
                    oldName = configOld.getName(),
                    newName = configNew.getName();

            try {
                replaceNestedConfigNames(oldName, newName);
            } catch (CoreException | LaunchConfigurationTypeNotFoundException e) {
                throw new RuntimeException(// todo log?
                        Messages.errorOnRenamingNestedConfigs(oldName, newName), e
                );
            }
        }
    }

    @Override public void launchConfigurationChanged(ILaunchConfiguration config) {}

    //todo impl?
    @Override public void launchConfigurationRemoved(ILaunchConfiguration config) {}
}