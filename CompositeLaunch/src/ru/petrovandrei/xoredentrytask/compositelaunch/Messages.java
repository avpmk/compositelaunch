package ru.petrovandrei.xoredentrytask.compositelaunch;

import java.util.Collection;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.osgi.util.NLS;

import static java.text.MessageFormat.format;

public class Messages extends NLS {

    public static String
            pluginName,
            errorOnWaitingForNestedConfigsComplete,
            configsNotFound,
            errorOnRenamingNestedConfigs,
            configNotComposite,
            noConfigsToStart,
            launch;

    public static String configNotComposite(ILaunchConfiguration config) {
        return format(configNotComposite, config.getName());
    }

    public static String configsNotFound(Collection<String> names) { //теперь не используется
        return format(configsNotFound, names);
    }

    public static String errorOnRenamingNestedConfigs(String oldName, String newName) {
        return format(errorOnRenamingNestedConfigs, oldName, newName);
    }

    static {
        NLS.initializeMessages("ru.petrovandrei.xoredentrytask.compositelaunch.messages", Messages.class); //$NON-NLS-1$
    }
}