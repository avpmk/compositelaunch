package ru.petrovandrei.xoredentrytask.compositelaunch.shell.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class SinglyLinkedNodeTest {

    @Test public void testEq_onEqualLists() {
        SinglyLinkedNode<String> rootA = new SinglyLinkedNode<>("root");
        SinglyLinkedNode<String> nodeA = rootA
                .createChild("A")
                .createChild("B")
                .createChild("C")
                .createChild("D")
                .createChild("E")
                .createChild("F");

        SinglyLinkedNode<String> rootB = new SinglyLinkedNode<>("root");
        SinglyLinkedNode<String> nodeB = rootB
                .createChild("A")
                .createChild("B")
                .createChild("C")
                .createChild("D")
                .createChild("E")
                .createChild("F");

        assertEquals(nodeA, nodeB);
    }

    @Test public void testEq_onEqualLists_builtInDifferntWay() {
        SinglyLinkedNode<String> leafA = new SinglyLinkedNode<>("F");
        leafA
                .createParent("E")
                .createParent("D")
                .createParent("C")
                .createParent("B")
                .createParent("A")
                .createParent("root");

        SinglyLinkedNode<String> rootB = new SinglyLinkedNode<>("root");
        SinglyLinkedNode<String> nodeB = rootB
                .createChild("A")
                .createChild("B")
                .createChild("C")
                .createChild("D")
                .createChild("E")
                .createChild("F");

        assertEquals(leafA, nodeB);
    }

    @Test public void testEq_onTheSameList() {
        SinglyLinkedNode<String> rootA = new SinglyLinkedNode<>("root");
        SinglyLinkedNode<String> nodeA = rootA
                .createChild("A")
                .createChild("B")
                .createChild("C")
                .createChild("D")
                .createChild("E")
                .createChild("F");

        assertEquals(nodeA, nodeA);
    }

    @Test public void testEq_onNotEqualLists() {
        SinglyLinkedNode<String> rootA = new SinglyLinkedNode<>("root");
        SinglyLinkedNode<String> nodeA = rootA
                .createChild("A")
                .createChild("B")
                .createChild("C")
                .createChild("D")
                .createChild("E")
                .createChild("F");

        SinglyLinkedNode<String> rootB = new SinglyLinkedNode<>("root");
        SinglyLinkedNode<String> nodeB = rootB
                .createChild("A")
                .createChild("B")
                .createChild("C")
                .createChild("D")
                .createChild("E");

        assertNotSame(nodeA, nodeB);
    }
}